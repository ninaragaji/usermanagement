package com.example.ninaragaji_270425;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DBHelper(this);
        Button loginButton = findViewById(R.id.loginButton);
        Button registerButton = findViewById(R.id.registerButton);
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.loginButton) {
            Log.d("MainActivity", "Login Button");
            Intent intent = new Intent(MainActivity.this, Main.class);
            EditText usernameText = findViewById(R.id.usernameEditText);
            EditText passwordText = findViewById(R.id.passwordEditText);
            String username = usernameText.getText().toString();
            String password = passwordText.getText().toString();
            try {
                String userPassword = dbHelper.getPasswordOfUser(username);
                Log.d("User",username + " " + userPassword);
                if(Objects.equals(userPassword, password)) {
                    Log.d("MainActivity","Login successful");
                    intent.putExtra("username",username);
                    startActivity(intent);
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Enter valid credentials", Toast.LENGTH_LONG);
                    View toastView = toast.getView();
                    toastView.setBackgroundColor(getColor(R.color.error_red));
                    toast.show();
                    Log.d("MainActivity","Login failed");
                }

            } catch (Exception e) {
                Log.d("MainActivity",e.toString());
            }
        }else if(view.getId() == R.id.registerButton){
            Log.d("MainActivity", "Register Button");
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
        } else {
            Log.d("MainActivity", "default message");
        }
    }


}
package com.example.ninaragaji_270425;

import android.widget.Button;

public class Model {
    private String username;
    private String email;
    private Button removeMe;
    private DBHelper dbHelper;
    private String signedInUsername;

    public Model(String username, String email, Button removeMe, DBHelper dbHelper, String signedInUsername) {
        this.username = username;
        this.email = email;
        this.removeMe = removeMe;
        this.dbHelper = dbHelper;
        this.signedInUsername = signedInUsername;
    }

    public String getSignedInUsername() {
        return signedInUsername;
    }

    public void setSignedInUsername(String signedInUsername) {
        this.signedInUsername = signedInUsername;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Button getRemoveMe() {
        return removeMe;
    }

    public DBHelper getDBHelper() {
        return dbHelper;
    }

    public void setRemoveMe(Button removeMe) {
        this.removeMe = removeMe;
    }
}

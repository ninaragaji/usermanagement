package com.example.ninaragaji_270425;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<Model> list;

    public MyAdapter(Context mContext) {
        this.mContext = mContext;
        list = new ArrayList<>();
    }

    public void addElement(Model model) {
        list.add(model);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Model getItem(int i) {
        Model m = null;
        try {
            m = list.get(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void removeElementByIndex(int i) {
        list.remove(i);
        notifyDataSetChanged();
    }

    public void removeAllElements() {
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row, null);
            viewHolder = new ViewHolder();
            viewHolder.usernameViewItem = view.findViewById(R.id.usernameTxt);
            viewHolder.emailViewItem = view.findViewById(R.id.emailTxt);
            viewHolder.removeButtonItem = view.findViewById(R.id.removeBtn);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Model model = (Model) getItem(i);

        viewHolder.usernameViewItem.setText(model.getUsername());
        viewHolder.emailViewItem.setText(model.getEmail());
        if(!model.getUsername().equals(model.getSignedInUsername())){
            viewHolder.removeButtonItem.setVisibility(View.VISIBLE);
            viewHolder.removeButtonItem.setEnabled(true);
        }else{
            viewHolder.removeButtonItem.setVisibility(View.INVISIBLE);
            viewHolder.removeButtonItem.setEnabled(false);
        }

        viewHolder.removeButtonItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBHelper dbHelper = model.getDBHelper();
                dbHelper.deleteUser(model.getUsername());
                removeElementByIndex(i);
            }
        });
        return view;
    }

    static class ViewHolder {
        TextView usernameViewItem;
        TextView emailViewItem;
        Button removeButtonItem;
    }
}

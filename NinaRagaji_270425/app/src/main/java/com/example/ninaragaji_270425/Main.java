package com.example.ninaragaji_270425;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Main extends AppCompatActivity implements View.OnClickListener {
    private String signedUsername;
    Button logoutButton;
    DBHelper dbHelper = new DBHelper(this);
    MyAdapter adapter = new MyAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        logoutButton = findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(this);
        signedUsername = getIntent().getStringExtra("username");
        Log.d("Statistic", "username: " + signedUsername);

        ListView list = findViewById(R.id.lista);
        list.setAdapter(adapter);

        populateAdapter(adapter, dbHelper);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void populateAdapter(MyAdapter adapter, DBHelper dBHelper){
        try {
                User[] users = dBHelper.getAllUsers();
                if(users.length == 0) {
                    Log.d("Statistic", "Lista korisnika je prazna");
                }
                else {
                    for (User user : users) {
                        String username = user.getUsername();
                        String email = user.getEmail();

                        adapter.addElement(new Model(username, email, findViewById(R.id.removeBtn), dBHelper, signedUsername));
                    }
                }
        } catch (Exception e){
            Toast toast = Toast.makeText(getApplicationContext(), "Database error", Toast.LENGTH_LONG);
            View toastView = toast.getView();
            toastView.setBackgroundColor(getColor(R.color.error_red));
            toast.show();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.logoutButton) {
            Log.d("Main", "Logout Button");
            Intent intent = new Intent(Main.this, MainActivity.class);
            startActivity(intent);
        } else {
            Log.d("Main", "default message");
        }
    }
}

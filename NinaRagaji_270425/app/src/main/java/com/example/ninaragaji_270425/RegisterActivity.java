package com.example.ninaragaji_270425;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
        private DBHelper dBHelper;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.register_activity);
            dBHelper = new DBHelper(this);
            findViewById(R.id.registerButtonInActivity).setOnClickListener(this);
        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onClick(View view) {
            try {
                EditText usernameText = findViewById(R.id.usernameEditTextRegister);
                EditText passwordText = findViewById(R.id.passwordEditTextRegister);
                EditText emailText = findViewById(R.id.emailEditTextRegister);

                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();
                String email = emailText.getText().toString();

                User user = new User(username, password, email);
                dBHelper.add(user);
                Log.d("RegisterActivity", "Registration successful");
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);

            } catch (Exception e) {
                Toast toast = Toast.makeText(getApplicationContext(), "Enter valid credentials", Toast.LENGTH_LONG);
                View toastView = toast.getView();
                toastView.setBackgroundColor(getColor(R.color.error_red));
                toast.show();
                Log.d("RegisterActivity", e.toString());
                e.printStackTrace();
            }
        }

}

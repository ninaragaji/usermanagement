package com.example.ninaragaji_270425;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.jetbrains.annotations.Nullable;


public class DBHelper extends SQLiteOpenHelper {
    public static final String ID_COLUMN = "ID";
    public static final String USERNAME_COLUMN = "USERNAME";
    public static final String PASSWORD_COLUMN = "PASSWORD";
    public static final String EMAIL_COLUMN = "EMAIL";
    public static final String USER_TABLE = "USER";

    public DBHelper(@Nullable Context context) {
        super(context, "nina_database.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTableStatement = "CREATE TABLE " + USER_TABLE + " (" + ID_COLUMN + " TEXT PRIMARY KEY, " + USERNAME_COLUMN + " TEXT, " + PASSWORD_COLUMN + " TEXT, " + EMAIL_COLUMN + " TEXT)";
        sqLiteDatabase.execSQL(createTableStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {}

    public void add(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(ID_COLUMN, user.getId());
        cv.put(USERNAME_COLUMN, user.getUsername());
        cv.put(PASSWORD_COLUMN, user.getPassword());
        cv.put(EMAIL_COLUMN, user.getEmail());

        Log.d("DBHelper",cv.toString());

        db.insert(USER_TABLE,null, cv);
        close();
    }

    public void deleteUser(String username) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(USER_TABLE, USERNAME_COLUMN + " =?", new String[] {username});
        close();
    }

    public String getPasswordOfUser(String userName) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT password FROM " + USER_TABLE + " WHERE " + USERNAME_COLUMN + " = '" + userName +"'", null);

        if(!cursor.moveToFirst()) {
            return null;
        }
        String password = cursor.getString(0);


        ContentValues cv = new ContentValues();

        cv.put(PASSWORD_COLUMN, password);


        Log.d("DBHelper",cv.toString());
        close();
        return password;
    }

    public User[] getAllUsers() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + USER_TABLE, null);

        if (cursor.getCount() <= 0) {
            return null;
        }
        User[] users = new User[cursor.getCount()];
        int i = 0;
       for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
           users[i++] = getUser(cursor);
        }

      close();
     return users;
    }

    private User getUser(Cursor cursor) {
        String id = cursor.getString(cursor.getColumnIndexOrThrow(ID_COLUMN));
        String username = cursor.getString(cursor.getColumnIndexOrThrow(USERNAME_COLUMN));
        String password = cursor.getString(cursor.getColumnIndexOrThrow(PASSWORD_COLUMN));
        String email = cursor.getString(cursor.getColumnIndexOrThrow(EMAIL_COLUMN));

        return new User(id, username, password, email);
    }
}
